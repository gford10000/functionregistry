// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package functionRegistry

import (
	"fmt"
	"reflect"
	"strings"
	"testing"
)

func Test20(t *testing.T) {

	a := 1
	err := Register("Errors", "Error1", a) // Can only be a func, passing an int

	if err == nil {
		t.Error("Failed to generate error")
	} else if msg := fmt.Sprintf("Supplied interface is not a func (%s)", reflect.ValueOf(a).Kind().String()); msg != err.Error() {
		t.Errorf("Incorrect error message received: %s", err.Error())
	}
}

func Test21(t *testing.T) {

	namespace, name := "Errors", "Error2"

	_, err := Invoke(namespace, name, 1, 2, 3) // Non-existent function

	if err == nil {
		t.Error("Failed to generate error")
	} else if msg := fmt.Sprintf("Not found: (%s, %s)", namespace, name); msg != err.Error() {
		t.Errorf("Incorrect error message received: %s", err.Error())
	}
}

func Test22(t *testing.T) {

	namespace, name := "Errors", "Error3"

	f := func(i ...int) int {
		tot := 0
		for x := range i {
			tot += i[x]
		}
		return tot
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		g := func(i int) int { return -i }

		err := Register(namespace, name, g) // Attempt to overwrite existing

		if err == nil {
			t.Error("Failed to generate error")
		} else if msg := fmt.Sprintf("Already registered (%s, %s)", namespace, name); msg != err.Error() {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test23(t *testing.T) {

	namespace, name := "Errors", "Error4"

	f := func(i ...int) int {
		tot := 0
		for x := range i {
			tot += i[x]
		}
		return tot
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		err := Register(namespace, name, f) // Attempt to re-register f()

		if err == nil {
			t.Error("Failed to generate error")
		} else if msg := fmt.Sprintf("Already registered (%s, %s)", namespace, name); msg != err.Error() {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test24(t *testing.T) {

	namespace, name := "Errors", "Error5"

	f := func(s string, i ...int) (string, int) {
		tot := 0
		for x := range i {
			tot += i[x]
		}
		return s, tot
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name)

		if err == nil {
			t.Error("Failed to generate error")
		} else if err.Error() != "Too few args supplied: received 0, expected 2" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test26(t *testing.T) {

	namespace, name := "Errors", "Error6"

	f := func(s string) string {
		return s
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name, "Hello", "World")

		if err == nil {
			t.Error("Failed to generate error")
		} else if err.Error() != "Too many args supplied: received 2, expected 1" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test27(t *testing.T) {

	namespace, name := "Errors", "Error7"

	f := func(s string) string {
		return s
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name, true) // Wrong type of arg

		if err == nil {
			t.Error("Failed to generate error")
		} else if err.Error() != "interface conversion: interface is bool, not string" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test28(t *testing.T) {

	namespace, name := "Errors", "Error8"

	f := func(s string) string {
		return s
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name) // No args provided

		if err == nil {
			t.Error("Failed to generate error")
		} else if err.Error() != "Too few args supplied: received 0, expected 1" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test29(t *testing.T) {

	namespace, name := "Errors", "Error9"

	f := func(s string, i ...int) (string, int) {
		tot := 0
		for x := range i {
			tot += i[x]
		}
		return s, tot
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name, "Hello", "World") // Wrong arg to a dotdotdot

		if err == nil {
			t.Error("Failed to generate error")
		} else if err.Error() != "interface conversion: interface is string, not int" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test30(t *testing.T) {

	namespace, name := "Errors", "Error10"

	f := func(s string, i ...int) (string, int) {
		tot := 0
		for x := range i {
			tot += i[x]
		}
		return s, tot
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name, "Hello", 1, 2, 3, "World") // Bad type in dotdotdot

		if err == nil {
			t.Error("Failed to generate error")
		} else if err.Error() != "interface conversion: interface is string, not int" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test31(t *testing.T) {

	namespace, name := "Errors", "Error11"

	f := func(s string, i ...int) (string, int) {
		tot := 0
		for x := range i {
			tot += i[x]
		}
		return s, tot
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		v, err := Invoke(namespace, name, "Hello", 1, 2, 3) // Data ok

		if err != nil {
			t.Errorf("Unexpected error: %s", err.Error())
		} else {
			v0, _ := v.Value(0)
			v1, _ := v.Value(1)
			if v2, ok := v1.(int); (ok && v2 != 6) || v0 != "Hello" {
				t.Errorf("Incorrect result received: %v", v)
			}
		}
	}
}

func Test32(t *testing.T) {

	namespace, name := "Errors", "Error12"

	f := func(n int, d int) int {
		return n / d
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		v, err := Invoke(namespace, name, 1, 0) // Div By Zero in func, should be reported as a result error

		if err != nil {
			t.Errorf("Unexpected error reported: %s", err.Error())
		} else {

			if v.GetError() == nil {
				t.Error("Failed to report error")
			} else if v.GetError().Error() != "runtime error: integer divide by zero" {
				t.Errorf("Incorrect error message received: %v", v.GetError().Error())
			}
		}
	}
}

func Test33(t *testing.T) {

	var f = func(j string, m float64, n *float64, i ...[]float64) (string, float64, error) {
		tot := 0.0
		for offset := range i {
			for offset2 := range i[offset] {
				tot += i[offset][offset2]
			}
		}
		return j, m + *n*tot, nil
	}

	// Register the function
	err := Register("Tests", "Test1", f)
	if err != nil {
		t.Errorf("Register Error: %s", err.Error())
		return
	}

	// Dummy data
	s := "Hello, World"
	m := 3.0
	n := 2.0
	f1, f2 := []float64{1.0, 1.1, 1.2, 1.3}, []float64{2.9}

	// Invoke the calculation
	r, err := Invoke("Tests", "Test1", s, m, &n, f1, f2)

	if err != nil {
		t.Errorf("Invoke Error: %s", err.Error())
	} else {
		if r.GetError() != nil {
			t.Errorf("Function Error: %s", r.GetError().Error())
		} else {
			v0, _ := r.Value(0)
			v1, _ := r.Value(1)
			v2, ok2 := v0.(string)
			v3, ok3 := v1.(float64)
			if (ok2 && v2 != s) || (ok3 && v3 != 18.0) {
				t.Error("Incorrect result generated")
			}
		}
	}
}

func Test34(t *testing.T) {

	namespace, name := "Errors", "Error13"

	f := func(n int) int {
		return n
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name, nil) // Passing nil pointer to an int argument

		if err == nil {
			t.Error("Expected error but none reported")
		} else if err.Error() != "Passing nil to int" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test35(t *testing.T) {

	namespace, name := "Errors", "Error14"

	f := func(n int) int {
		return n
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		i := 2

		_, err := Invoke(namespace, name, &i) // Passing non-nil pointer to an int argument

		if err == nil {
			t.Error("Expected error but none reported")
		} else if err.Error() != "interface conversion: interface is *int, not int" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func checkNilTestResult(namespace string, name string, f interface{}, valueTest func(i interface{}) bool, t *testing.T) {

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f():", err)
	} else {
		v, err := Invoke(namespace, name, nil)

		if err != nil {
			t.Error("Unexpected error:", err)
		} else {
			if b, _ := v.IsNil(0); !b {
				v0, _ := v.Value(0)
				t.Errorf("Expected nil - got %v", v0)
			} else {
				v0, _ := v.Value(0)
				if valueTest != nil && valueTest(v0) {
					t.Errorf("Expected nil value - got %v (%s)", v0, reflect.TypeOf(v0))
				}
			}
		}
	}
}

func Test36(t *testing.T) {

	namespace, name := "Errors", "Error15"

	f := func(n []int) []int {
		return n
	}

	checkNilTestResult(namespace, name, f, nil, t)
}

func Test37(t *testing.T) {

	namespace, name := "Errors", "Error16"

	f := func(n *int) *int {
		return n
	}

	g := func(i interface{}) bool {
		return i != (*int)(nil)
	}

	checkNilTestResult(namespace, name, f, g, t)
}

func Test38(t *testing.T) {

	namespace, name := "Errors", "Error17"

	f := func(n chan int) chan int {
		return n
	}

	g := func(i interface{}) bool {
		return i != (chan int)(nil)
	}

	checkNilTestResult(namespace, name, f, g, t)
}

func Test39(t *testing.T) {

	namespace, name := "Errors", "Error18"

	f := func(n map[string]int) map[string]int {
		return n
	}

	checkNilTestResult(namespace, name, f, nil, t)
}

func Test40(t *testing.T) {

	namespace, name := "Errors", "Error19"

	type test40 struct {
		a int
		b float64
	}

	f := func(n test40) test40 {
		return n
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		_, err := Invoke(namespace, name, nil) // Passing nil pointer to an test40 argument

		if err == nil {
			t.Error("Expected error but none reported")
		} else if err.Error() != "Passing nil to functionRegistry.test40" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

type test4142 struct {
	a int64
	b int64
}

func (t *test4142) Eval(i int64) int64 {
	return t.a*t.b + i
}

func test41Register(namespace string, name string) error {

	a := new(test4142)
	a.a = 3
	a.b = 7

	return Register(namespace, name, a.Eval)
}

func Test41(t *testing.T) {

	namespace, name := "Tests", "Test2"

	err := test41Register(namespace, name)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		// Demonstrate that an instance based function can be registered and invoked

		var i int64 = 5

		v, err := Invoke(namespace, name, i)

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(int64); ok && v1 != 26 {
						t.Errorf("Unexpected result: expected 26, got %v", v0)
					}
				}
			}
		} else if err.Error() != "Passing nil to functionRegistry.test40" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func test42Register(a *test4142, namespace string, name string) error {

	a.a = 3
	a.b = 7

	return Register(namespace, name, a.Eval)
}

func Test42(t *testing.T) {

	namespace, name := "Tests", "Test3"

	a := new(test4142)

	err := test42Register(a, namespace, name)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		// Demonstrate that the behaviour of an instance based function that has registered
		// can be altered after registration

		var i int64 = 5
		a.a = 0

		v, err := Invoke(namespace, name, i)

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(int64); ok && v1 != 5 {
						t.Errorf("Unexpected result: expected 5, got %v", v0)
					}
				}
			}
		} else if err.Error() != "Passing nil to functionRegistry.test40" {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test43(t *testing.T) {

	namespace, name := "Tests", "Test4"

	f := func(n int64) int64 {
		return n
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		// Demonstrate casting works as expected: int can be promoted to int64

		var i int = 5

		v, err := Invoke(namespace, name, i)

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(int64); ok && v1 != 5 {
						t.Errorf("Unexpected result: expected 5, got %v", v0)
					}
				}
			}
		} else {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test44(t *testing.T) {

	namespace, name := "Tests", "Test5"

	f := func(n float64) float64 {
		return n
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		// Demonstrate casting works as expected: int can be promoted to float64

		var i int = 5

		v, err := Invoke(namespace, name, i)

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(float64); ok && v1 != 5 {
						t.Errorf("Unexpected result: expected 5, got %v", v0)
					}
				}
			}
		} else {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test45(t *testing.T) {

	namespace, name := "Tests", "Test6"

	f := func(n complex64) complex64 {
		return n
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f()")
	} else {

		// Demonstrate casting works as expected: complex128 can be cast to complex64

		var i complex128 = 5 + 1i

		v, err := Invoke(namespace, name, i)

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(complex64); ok && v1 != 5+1i {
						t.Errorf("Unexpected result: expected 5, got %v", v0)
					}
				}
			}
		} else {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test46(t *testing.T) {

	namespace, name := "Tests", "Test7"

	f := func() string {
		return "Hello, World"
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f():", err)
	} else {

		// Demonstrates function with no arguments

		v, err := Invoke(namespace, name)

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(string); ok && v1 != "Hello, World" {
						t.Errorf("Unexpected result: expected Hello, World, got %v", v0)
					}
				}
			}
		} else {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test47(t *testing.T) {

	namespace, name := "Tests", "Test8"

	f := func(args ...string) string {
		if len(args) == 0 {
			return "Hello, World"
		}
		return "Hello " + strings.Join(args, ", ")
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f():", err)
	} else {

		// Demonstrates function with only variadic arg

		v, err := Invoke(namespace, name)

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(string); ok && v1 != "Hello, World" {
						t.Errorf("Unexpected result: expected Hello, World, got %v", v0)
					}
				}
			}
		} else {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test48(t *testing.T) {

	namespace, name := "Tests", "Test9"

	f := func(args ...string) string {
		if len(args) == 0 {
			return "Hello, World"
		}
		return "Hello " + strings.Join(args, ", ")
	}

	err := Register(namespace, name, f)

	if err != nil {
		t.Error("Unexpected error registering f():", err)
	} else {

		// Demonstrates function with only variadic arg (with some supplied)

		v, err := Invoke(namespace, name, "Joe", "Fred", "Colin")

		if err == nil {

			if v.GetError() != nil {
				t.Errorf("Unexpected error inside calculation: %s", v.GetError().Error())
			} else {
				if b, _ := v.IsNil(0); b {
					t.Error("Unexpected nil value returned")
				} else {
					v0, _ := v.Value(0)
					if v1, ok := v0.(string); ok && v1 != "Hello Joe, Fred, Colin" {
						t.Errorf("Unexpected result: expected Hello Joe, Fred, Colin; got %v", v0)
					}
				}
			}
		} else {
			t.Errorf("Incorrect error message received: %v", err.Error())
		}
	}
}

func Test49(t *testing.T) {

	if IsRegistered("AAAA", "Aardvark") {
		t.Error("Incorrectly indicating non-existent function is registered")
	} else {

		f := func() string {
			return "Aardvarks Rock!"
		}

		Register("AAAA", "Aardvark", f)

		if !IsRegistered("AAAA", "Aardvark") {
			t.Error("Incorrectly indicating registered function is not registered")
		}
	}
}

func Test50(t *testing.T) {

	f1 := func() string {
		return "Aardvarks Rock!"
	}

	f2 := func() (string, error) {
		return f1(), nil
	}

	f3 := func(s string) (string, error) {
		return s, nil
	}

	f4 := func(s string, others ...string) (string, error) {
		return s + strings.Join(others, ", "), nil
	}

	f5 := func(others ...string) (string, error) {
		return strings.Join(others, ", "), nil
	}

	f6 := func() {
	}

	Register("Testing", "Signature1", f1)
	Register("Testing", "Signature2", f2)
	Register("Testing", "Signature3", f3)
	Register("Testing", "Signature4", f4)
	Register("Testing", "Signature5", f5)
	Register("Testing", "Signature6", f6)

	if s, _ := Signature("Testing", "Signature1"); s != "func() string" {
		t.Errorf("Bad signature: expected func() string, got %s", s)
	}

	if s, _ := Signature("Testing", "Signature2"); s != "func() (string, error)" {
		t.Errorf("Bad signature: expected func() (string, error), got %s", s)
	}

	if s, _ := Signature("Testing", "Signature3"); s != "func(string) (string, error)" {
		t.Errorf("Bad signature: expected func(string) (string, error), got %s", s)
	}

	if s, _ := Signature("Testing", "Signature4"); s != "func(string, ...string) (string, error)" {
		t.Errorf("Bad signature: expected func(string, ...string) (string, error), got %s", s)
	}

	if s, _ := Signature("Testing", "Signature5"); s != "func(...string) (string, error)" {
		t.Errorf("Bad signature: expected func(...string) (string, error), got %s", s)
	}

	if s, _ := Signature("Testing", "Signature6"); s != "func()" {
		t.Errorf("Bad signature: expected func(), got %s", s)
	}
}

func testOffsetNil(r Result, i int, max int, expectError bool, t *testing.T) {

	_, err := r.IsNil(i)

	err1 := fmt.Sprintf("IsNil: Requested offset (%v) is out of bounds for Result (length=%v)", i, max)

	if expectError {
		if err == nil {
			t.Error("Expected an error - didn't get one")
		} else if err.Error() != err1 {
			t.Errorf("Expected %s, got %s", err1, err.Error())
		}
	} else {
		if err != nil {
			t.Errorf("Did not expect an error - but got %v", err)
		}
	}
}

func Test51(t *testing.T) {

	f := func() string {
		return "Aardvarks Rock!"
	}

	Register("Testing", "Offset1", f)

	r, _ := Invoke("Testing", "Offset1")

	testOffsetNil(r, 0, 1, false, t)
	testOffsetNil(r, 2, 1, true, t)
	testOffsetNil(r, -2, 1, true, t)
}

func testOffsetValue(r Result, i int, max int, expectError bool, t *testing.T) {

	_, err := r.Value(i)

	err1 := fmt.Sprintf("Value: Requested offset (%v) is out of bounds for Result (length=%v)", i, max)

	if expectError {
		if err == nil {
			t.Error("Expected an error - didn't get one")
		} else if err.Error() != err1 {
			t.Errorf("Expected %s, got %s", err1, err.Error())
		}
	} else {
		if err != nil {
			t.Errorf("Did not expect an error - but got %v", err)
		}
	}
}

func Test52(t *testing.T) {

	f := func() string {
		return "Aardvarks Rock!"
	}

	Register("Testing", "Offset2", f)

	r, _ := Invoke("Testing", "Offset2")

	testOffsetValue(r, 0, 1, false, t)
	testOffsetValue(r, 2, 1, true, t)
	testOffsetValue(r, -2, 1, true, t)
}

func Test53(t *testing.T) {

	type t53 struct {
		a int
		b string
	}

	f := func(v *t53) t53 {
		return *v
	}

	Register("Testing", "Struct1", f)

	var input = t53{42, "Hello, World"}

	r, err := Invoke("Testing", "Struct1", &input)

	if err != nil {
		t.Errorf("Unexpected error - %v", err)
	} else {
		if r.GetError() != nil {
			t.Errorf("Unexpected result error - %v", r.GetError())
		} else {
			v0, err := r.Value(0)
			if err != nil {
				t.Errorf("Unexpected Result.Value(0) error: %v", err)
			} else if v1, ok := v0.(t53); ok && v1 != input {
				t.Errorf("Unexpected Result.Value(0) value: %v", v1)
			}
		}
	}
}

type T54 interface {
	GetA() int
	GetB() string
}

type t54 struct {
	a int
	b string
}

func (a *t54) GetA() int {
	return a.a
}

func (a *t54) GetB() string {
	return a.b
}

func Test54(t *testing.T) {

	f := func(v T54) T54 {
		return v
	}

	Register("Testing", "Interface1", f)

	var input = t54{42, "Hello, World"}

	r, err := Invoke("Testing", "Interface1", &input)

	if err != nil {
		t.Errorf("Unexpected error - %v", err)
	} else {
		if r.GetError() != nil {
			t.Errorf("Unexpected result error - %v", r.GetError())
		} else {
			v0, err := r.Value(0)
			if err != nil {
				t.Errorf("Unexpected Result.Value(0) error: %v", err)
			} else if v1, ok := v0.(T54); ok && (v1.GetA() != input.GetA() || v1.GetB() != input.GetB()) {
				t.Errorf("Unexpected Result.Value(0) value: %v", v1)
			}
		}
	}
}

type T55 interface {
	GetA() int
	GetB() string
}

type t55 struct {
	a int
	b string
}

func (a *t55) GetA() int {
	return a.a
}

func (a *t55) GetB1() string {
	return a.b
}

func Test55(t *testing.T) {

	f := func(v T55) T55 {
		return v
	}

	Register("Testing", "Interface2", f)

	var input = t55{42, "Hello, World"}

	_, err := Invoke("Testing", "Interface2", &input)

	if err != nil {
		if err.Error() != "interface conversion: interface is *functionRegistry.t55, not functionRegistry.T55" {
			t.Errorf("Unexpected error - %v", err)
		}
	} else {
		t.Error("Should have received an error due to type mis-match")
	}
}
