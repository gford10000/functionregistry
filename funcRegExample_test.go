// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package functionRegistry

import (
	"fmt"
)

func ExampleInvoke() {

	f := func() string {
		return "Hello, World"
	}

	Register("Global", "Hello", f)

	result, _ := Invoke("Global", "Hello")

	v, _ := result.Value(0)

	fmt.Printf("%v", v)
	// Output: Hello, World
}

func ExampleSignature() {

	f := func(in string, values ...int32) (out string, tot int32) {

		out = in

		for i := range values {
			tot += values[i]
		}

		return
	}

	Register("Global", "Summarizer", f)

	v, _ := Signature("Global", "Summarizer")

	fmt.Printf("%v", v)
	// Output: func(string, ...int32) (string, int32)

}
