// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// functionRegistry package allows functions to be invoked by name, with a runtime verification
// of arguments and return values.
package functionRegistry

import (
	"errors"
	"fmt"
	"reflect"
	"runtime"
	"strings"
	"sync"
)

// Registry singleton
var registry map[string]*fnManager

// Lock for map
var lk sync.RWMutex

// Ensures the registry exists prior to first use
func init() {
	registry = make(map[string]*fnManager)
}

// panicHelperf simplifies the creation of errors from formatted strings
func panicHelperf(format string, v ...interface{}) {
	panicHelper(fmt.Sprintf(format, v...))
}

// panicHelper simplifies the creation of errors from strings
func panicHelper(str string) {
	panic(errors.New(str))
}

// Register adds the supplied function to the registry singleton at the specified namespace/name location.
// An error will be raised if:
//
// - the supplied interface is not a function
//
// - a function has already been registered for the namespace/name
func Register(namespace string, name string, fn interface{}) (err error) {

	defer func() {
		if i := recover(); i != nil {
			if e, ok := i.(runtime.Error); ok {
				err = e
			} else if e1, ok := i.(error); ok {
				err = e1
			} else if s, ok := i.(string); ok {
				err = errors.New(s)
			} else {
				panic(i)
			}
		}
	}()

	f := new(fnManager)
	f.init(fn)
	key := createKey(namespace, name)

	lk.Lock()
	defer func() {
		lk.Unlock()
	}()

	if registry[key] != nil {
		panicHelperf("Already registered (%s, %s)", namespace, name)
	}
	registry[key] = f

	return
}

// IsRegistered returns true if a function has already been registered for the namespace/name
func IsRegistered(namespace string, name string) bool {

	key := createKey(namespace, name)

	lk.RLock()
	defer func() {
		lk.RUnlock()
	}()

	return registry[key] != nil
}

// Signature returns the function signature for the specified namespace/name location.
// A error will be raised if there is no registered function at that location
func Signature(namespace string, name string) (s string, err error) {

	defer func() {
		if i := recover(); i != nil {
			if e, ok := i.(runtime.Error); ok {
				panic(e)
			}
			err = i.(error)
		}
	}()

	s = get(namespace, name).String()

	return
}

// Result contains the outcome of calling Invoke() for a given namespace/name location.
type Result interface {
	GetError() error                  // Contains the error if one was raised by the function invoked
	NumValues() int                   // The number of non-error return values
	Value(i int) (interface{}, error) // Access to the return values
	IsNil(i int) (bool, error)        // Whether the returned value is nil
}

// Invoke calls the function at the specified location, passing the supplied arguments.
// A error will be raised if:
//
// - there is no registered function at that location
//
// - there is an argument mismatch between the declared function and the supplied arguments
func Invoke(namespace string, name string, args ...interface{}) (result Result, err error) {

	defer func() {
		if i := recover(); i != nil {
			if e, ok := i.(runtime.Error); ok {
				err = e
			} else if e1, ok := i.(error); ok {
				err = e1
			} else if s, ok := i.(string); ok {
				err = errors.New(s)
			} else {
				panic(i)
			}
		}
	}()

	result = get(namespace, name).invoke(args)

	return
}

// get retrieves the function for the specified namespace/name location.
func get(namespace string, name string) *fnManager {

	lk.RLock()
	defer func() {
		lk.RUnlock()
	}()

	f := registry[createKey(namespace, name)]
	if f == nil {
		panicHelperf("Not found: (%s, %s)", namespace, name)
	}
	return f
}

// createKey standardises the creation of keys for the registry
func createKey(namespace string, name string) string {
	return strings.Join([]string{namespace, name}, "/")
}

// argConverter provides the conversion approach for a function argument
type argConverter struct {
	ty reflect.Type
}

// init initialises the argConverter for the specified type
func (a *argConverter) init(t reflect.Type) {
	a.ty = t
}

// convert invokes the Converter function with the supplied interface to retrieve a
// reflect.Value of the correct type
func (a *argConverter) convert(i interface{}) reflect.Value {

	if i == nil {
		switch a.ty.Kind() {
		case reflect.Ptr, reflect.Slice, reflect.Chan, reflect.Array, reflect.Func,
			reflect.Interface, reflect.Map:
			{
				return reflect.Zero(a.ty)
			}
		default:
			{
				panicHelperf("Passing nil to %s", a.ty.String())
			}
		}
	}

	v := reflect.ValueOf(i)

	defer func() {
		if i := recover(); i != nil {
			panicHelperf("interface conversion: interface is %s, not %s", v.Type().String(), a.ty.String())
		}
	}()

	return v.Convert(a.ty)
}

// result is the package's implementation of the Result interface
type result struct {
	err  error
	vals []interface{}
}

// GetError returns the error generated during function evaluation
func (r *result) GetError() error {
	return r.err
}

// NumValues returns the number of values (other than an error) that were returned from the function evaluation
func (r *result) NumValues() int {
	if r.vals == nil {
		panicHelper("No values available")
	}
	return len(r.vals)
}

// Value provides access to the return value at the specified offset
func (r *result) Value(i int) (v interface{}, e error) {
	if r.vals == nil {
		e = errors.New("Value: No values available")
	} else if i < 0 || i > r.NumValues() {
		e = errors.New(fmt.Sprintf("Value: Requested offset (%v) is out of bounds for Result (length=%v)", i, r.NumValues()))
	} else {
		v = r.vals[i]
	}
	return
}

// IsNil allows a test for nil at the specified offset
func (r *result) IsNil(i int) (b bool, e error) {
	if r.vals == nil {
		e = errors.New("IsNil: No values available")
	} else if i < 0 || i > r.NumValues() {
		e = errors.New(fmt.Sprintf("IsNil: Requested offset (%v) is out of bounds for Result (length=%v)", i, r.NumValues()))
	} else {
		v := reflect.ValueOf(r.vals[i])
		switch v.Kind() {
		case reflect.Chan, reflect.Func, reflect.Map, reflect.Ptr, reflect.Interface, reflect.Slice:
			{
				b = v.IsNil()
			}
		default:
			{
				b = false
			}
		}
	}
	return
}

// fnManager manages the invocation of a given function.
// The function is inspected on registration to extract the Type information on its arguments
// and return values, to save time during invocation requests.
type fnManager struct {
	fn        reflect.Value
	ins       []argConverter
	outs      []reflect.Type
	variadic  bool
	nInFixed  int
	errOffset int
}

// init is called when the function is being registered
func (f *fnManager) init(i interface{}) {

	fn := reflect.ValueOf(i)

	if fn.Kind() != reflect.Func {
		panicHelperf("Supplied interface is not a func (%s)", fn.Kind().String())
	}

	t := fn.Type()

	f.fn = fn
	f.variadic = t.IsVariadic()
	f.ins = make([]argConverter, t.NumIn())
	f.outs = make([]reflect.Type, t.NumOut())
	f.nInFixed += len(f.ins)

	if t.NumIn() > 0 {
		for offset := 0; offset < t.NumIn()-1; offset++ {
			f.ins[offset].init(t.In(offset))
		}
		if f.variadic {
			f.nInFixed -= 1
			f.ins[t.NumIn()-1].init(t.In(t.NumIn() - 1).Elem())
		} else {
			f.ins[t.NumIn()-1].init(t.In(t.NumIn() - 1))
		}
	}

	errorType := reflect.TypeOf((*error)(nil)).Elem()
	f.errOffset = -1
	for offset := 0; offset < t.NumOut(); offset++ {

		f.outs[offset] = t.Out(offset)

		if f.outs[offset] == errorType {
			f.errOffset = offset
		}
	}
}

// invoke actually performs the underlying function call
func (f *fnManager) invoke(args []interface{}) (ret *result) {

	if f.nInFixed > len(args) {
		panicHelperf("Too few args supplied: received %d, expected %d", len(args), len(f.ins))
	}

	if !f.variadic && f.nInFixed != len(args) {
		panicHelperf("Too many args supplied: received %d, expected %d", len(args), len(f.ins))
	}

	ins := make([]reflect.Value, 0, len(args))
	for i := 0; i < f.nInFixed; i++ {
		ins = append(ins, f.ins[i].convert(args[i]))
	}
	for i := f.nInFixed; i < len(args); i++ {
		ins = append(ins, f.ins[f.nInFixed].convert(args[i]))
	}

	ret = new(result) // Create result container

	// All subsequent errors will be due to issues with the call to the function itself,
	// so add this deferred function to capture them and add them to the results.
	defer func() {
		if i := recover(); i != nil {
			if e, ok := i.(runtime.Error); ok {
				ret.err = e
			} else {
				ret.err = i.(error)
			}
		}
	}()

	outs := f.fn.Call(ins)

	// Inspect the outs
	if outs != nil && len(outs) == len(f.outs) {
		if f.errOffset != -1 {
			ret.err, _ = outs[f.errOffset].Interface().(error)
			ret.vals = make([]interface{}, len(outs)-1)
			j := 0
			for i := 0; i < len(outs); i++ {
				if i != f.errOffset {
					ret.vals[j] = outs[i].Interface()
					j += 1
				}
			}
		} else {
			ret.vals = make([]interface{}, len(outs))
			for i := 0; i < len(outs); i++ {
				ret.vals[i] = outs[i].Interface()
			}
		}
	} else {
		if outs != nil {
			ret.err = errors.New(fmt.Sprintf("Error invoking function - unexpected output: expected %d, got %d", len(f.outs), len(outs)))
		} else {
			ret.err = errors.New(fmt.Sprintf("Error invoking function - unexpected output: expected %d, but nothing returned", len(f.outs)))
		}
	}

	return
}

// Returns the signature of the function
func (f *fnManager) String() string {

	nIn := len(f.ins)
	nOut := len(f.outs)

	s := "func("
	for i := 0; i < nIn-1; i++ {
		s += f.ins[i].ty.String() + ", "
	}
	if nIn > 0 {
		if f.variadic {
			s += "..."
		}
		s += f.ins[nIn-1].ty.String()
	}
	s += ")"

	if nOut > 0 {
		if nOut == 1 {
			s += " " + f.outs[0].String()
		} else {
			s += " ("
			for i := 0; i < nOut-1; i++ {
				s += f.outs[i].String() + ", "
			}
			s += f.outs[nOut-1].String() + ")"
		}
	}

	return s
}
