# functionRegistry - allows functions to be invoked by name, with a runtime verification of arguments and return values

The motivation for this package is to be able to declare functions during initialisation and then invoke them by name at a later time, with arbitrary arguments that are verified at runtime.

## Getting started

Current version of the library requires a latest stable Go release. If you don't have the Go compiler installed, read the official [Go install guide](http://golang.org/doc/install).

Use go tool to install the package in your packages tree:

```
go get bitbucket.com/gford10000/functionRegistry
```

Then you can use it in import section of your Go programs:

```go
import "bitbucket.com/gford10000/functionRegistry"
```

## Basic Example

```go
package main

import (
	"bitbucket.com/gford10000/functionRegistry"
	"fmt"
	"strings"
)

func hello(names ...string) string {
	if len(names) == 0 {
		return "Hello, World"
	}

	return "Hello " + strings.Join(names, ", ")
}

func main() {

	functionRegistry.Register("", "HelloFn", hello)

	v, _ := functionRegistry.Invoke("", "HelloFn", "Simon", "David")

	v0, _ := v.Value(0) // retrieve value from Result

	fmt.Printf("%v", v0)	// Prints Hello Simon, David
}
```

