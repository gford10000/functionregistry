// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package functionRegistry

import (
	"fmt"
	"testing"
)

var registered = false

func register(b *testing.B) bool {

	f0 := func() int {
		return 1
	}

	f1 := func(i int) int {
		return i
	}

	f2 := func(i1 int, i2 int) int {
		return i1 + i2
	}

	f3 := func(i1 int, i2 int, i3 int) int {
		return i1 + i2 + i3
	}

	f4 := func(i1 int, i2 int, i3 int, i4 int) int {
		return i1 + i2 + i3 + i4
	}

	f5 := func(i1 int, i2 int, i3 int, i4 int, i5 int) int {
		return i1 + i2 + i3 + i4 + i5
	}

	f6 := func(i1 int, i2 int, i3 int, i4 int, i5 int, i6 int) int {
		return i1 + i2 + i3 + i4 + i5 + i6
	}

	f7 := func(i1 int, i2 int, i3 int, i4 int, i5 int, i6 int, i7 int) int {
		return i1 + i2 + i3 + i4 + i5 + i6 + i7
	}

	f8 := func(i1 int, i2 int, i3 int, i4 int, i5 int, i6 int, i7 int, i8 int) int {
		return i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8
	}

	f9 := func(i1 int, i2 int, i3 int, i4 int, i5 int, i6 int, i7 int, i8 int, i9 int) int {
		return i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8 + i9
	}

	f10 := func(i1 int, i2 int, i3 int, i4 int, i5 int, i6 int, i7 int, i8 int, i9 int, i10 int) int {
		return i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8 + i9 + i10
	}

	funcs := []interface{}{f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10}

	for i := 0; i < len(funcs); i++ {
		err := Register("Benchmark", fmt.Sprintf("Bench%d", i), funcs[i])

		if err != nil {
			b.Errorf("Unexpected error during Register(): %s", err.Error())
			return false
		}
	}

	return true
}

func runControl1(b *testing.B) {

	f1 := func(i int) int {
		return i
	}

	var v int64
	for i := 0; i < b.N; i++ {

		v += int64(f1(1))
	}
}

func runControl10(b *testing.B) {

	f10 := func(i1 int, i2 int, i3 int, i4 int, i5 int, i6 int, i7 int, i8 int, i9 int, i10 int) int {
		return i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8 + i9 + i10
	}

	var v int64
	var i1, i2, i3, i4, i5, i6, i7, i8, i9, i10 int = 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
	for i := 0; i < b.N; i++ {

		v += int64(f10(i1, i2, i3, i4, i5, i6, i7, i8, i9, i10))
	}
}

func runTest(b *testing.B, namespace string, name string, args ...int) {
	b.StopTimer()

	if !registered {

		register(b)

		registered = true
	}

	is := make([]interface{}, len(args))
	for i := 0; i < len(args); i++ {
		is[i] = args[i]
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		_, err := Invoke(namespace, name, is...)

		if err != nil {
			b.Errorf("Unexpected error during Invoke(): %s", err.Error())
			break
		}
	}
}

// Assess the impact of adding arguments to the call

func BenchmarkCtrl1(b *testing.B)  { runControl1(b) }
func BenchmarkCtrl10(b *testing.B) { runControl10(b) }
func Benchmark0(b *testing.B)      { runTest(b, "Benchmark", "Bench0") }
func Benchmark1(b *testing.B)      { runTest(b, "Benchmark", "Bench1", 1) }
func Benchmark2(b *testing.B)      { runTest(b, "Benchmark", "Bench2", 1, 2) }
func Benchmark3(b *testing.B)      { runTest(b, "Benchmark", "Bench3", 1, 2, 3) }
func Benchmark4(b *testing.B)      { runTest(b, "Benchmark", "Bench4", 1, 2, 3, 4) }
func Benchmark5(b *testing.B)      { runTest(b, "Benchmark", "Bench5", 1, 2, 3, 4, 5) }
func Benchmark6(b *testing.B)      { runTest(b, "Benchmark", "Bench6", 1, 2, 3, 4, 5, 6) }
func Benchmark7(b *testing.B)      { runTest(b, "Benchmark", "Bench7", 1, 2, 3, 4, 5, 6, 7) }
func Benchmark8(b *testing.B)      { runTest(b, "Benchmark", "Bench8", 1, 2, 3, 4, 5, 6, 7, 8) }
func Benchmark9(b *testing.B)      { runTest(b, "Benchmark", "Bench9", 1, 2, 3, 4, 5, 6, 7, 8, 9) }
func Benchmark10(b *testing.B)     { runTest(b, "Benchmark", "Bench10", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10) }

var bench11, bench12, bench13, bench14 = false, false, false, false

func registerFunc(flag *bool, numFns int) {

	if !*flag {
		f5 := func(i1 int, i2 int, i3 int, i4 int, i5 int) int {
			return i1 + i2 + i3 + i4 + i5
		}

		for i := 0; i < numFns; i++ {
			Register("Benchmark", fmt.Sprintf("Fill%d", i), f5)
		}

		*flag = true
	}
}

func runTest2(b *testing.B, flag *bool, numFns int) {

	b.StopTimer()

	registerFunc(flag, numFns)

	name := fmt.Sprintf("Fill%d", numFns-1)

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		_, err := Invoke("Benchmark", name, 1, 2, 3, 4, 5)

		if err != nil {
			b.Errorf("Unexpected error during Invoke(): %s", err.Error())
			break
		}
	}
}

// Assess the impact of the number of registered functions, using a function with 5 args

func Benchmark11(b *testing.B) { runTest2(b, &bench11, 100) }
func Benchmark12(b *testing.B) { runTest2(b, &bench12, 500) }
func Benchmark13(b *testing.B) { runTest2(b, &bench13, 1000) }
func Benchmark14(b *testing.B) { runTest2(b, &bench14, 5000) }
